# -*- coding: utf-8 -*-
import json
from django.contrib.gis.measure import D
from django.contrib.gis.geos import Point

from tastypie.authorization import Authorization
from tastypie.serializers import Serializer
from tastypie import fields
from tastypie.contrib.gis.resources import ModelResource
from tastypie.resources import ALL_WITH_RELATIONS

from .models import Bank, BankType


class BankTypeResource(ModelResource):
    class Meta:
        resource_name = 'banktype'
        queryset = BankType.objects.all()


class BankResource(ModelResource):
    '''
    TODO: Tratar nome, em criação
    Exemplo:
    GET:
    Distância default = 50km
    http://host/api/v1/bank/?point={"coordinates": [-34.873058, -8.063426]}

    http://host/api/v1/bank/?point={"coordinates": [-34.873058, -8.063426]}&
    distance={"km": 0.2}

    Com filtro por banco:
    http://host/api/v1/bank/?point={"coordinates": [-34.873058, -8.063426]}&
    distance={"km": 0.2}&bank=4

    POST:
    import json
    import requests

    url = 'http://localhost:8000/api/v1/bank/'
    d = {
        u'address': u'Avenida X de Olinda, 215 - Recife Velho, Recife',
        u'location': {
            u'coordinates': [-34.87304, -8.063425], u'type': u'Point',
        },
        u'bank': {u'pk': 2},
    }

    headers = {'content-type': 'application/json'}
    requests.post(url, data=json.dumps(d), headers=headers)

    '''
    bank = fields.ForeignKey(BankTypeResource, 'bank', full=True)

    class Meta:
        queryset = Bank.objects.all()
        resource_name = 'bank'
        #list_allowed_methods = ['get', 'post']
        authorization= Authorization()
        serializer = Serializer(formats=['jsonp', 'json'])
        filtering = {
            'bank': ALL_WITH_RELATIONS,
        }

    def get_object_list(self, request):
        point = json.loads(request.GET['point'])
        point = Point(*point['coordinates'])

        distance = json.loads(request.GET.get('distance', '[]')) or {'km': 50}

        qs = super(BankResource, self).get_object_list(request)

        qs = qs.filter(location__distance_lte=(point, D(**distance)))
        return qs.distance(point).order_by('distance')
