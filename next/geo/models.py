# -*- coding: utf-8 -*-
from django.contrib.gis.db import models
from django.contrib.gis.measure import D
from django.template.defaultfilters import slugify


class BankType(models.Model):
    name = models.CharField(u'Nome do Banco', max_length=20)
    slug = models.SlugField(unique=True, blank=True)
    logo = models.ImageField(upload_to=u'uploads/bank/', null=True, blank=True)

    def __unicode__(self):
        return self.slug

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(BankType, self).save(*args, **kwargs)


class Bank(models.Model):
    bank = models.ForeignKey('geo.BankType')

    location = models.PointField(
        u'Posição', help_text=u'Longitude (x) Latitude (y)', unique=True,
    )
    address = models.CharField(u'Endereço', max_length=255)

    objects = models.GeoManager()

    def __unicode__(self):
        return u'{0} - {1}'.format(self.bank, self.location)

    def nearby(self, point, dist):
        return self.objects.filter(location__distance_lte=(point, D(km=dist)))
