# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models
from django.template.defaultfilters import slugify


class Migration(DataMigration):

    def forwards(self, orm):
        "Troca atributos de nome para instância de BankType"
        banks = orm['geo.Bank'].objects.all()
        for bank in banks:
            if not orm['geo.BankType'].objects.filter(slug=bank.name).exists():
                bank.name = slugify(bank.name)
            bank.bank = orm['geo.BankType'].objects.get(slug=bank.name)
            bank.save()

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        'geo.bank': {
            'Meta': {'object_name': 'Bank'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'bank': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['geo.BankType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.contrib.gis.db.models.fields.PointField', [], {'unique': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'geo.banktype': {
            'Meta': {'object_name': 'BankType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'blank': 'True'})
        }
    }

    complete_apps = ['geo']
    symmetrical = True
