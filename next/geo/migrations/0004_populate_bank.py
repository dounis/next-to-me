# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import DataMigration

from django.db import models
from django.template.defaultfilters import slugify


class Migration(DataMigration):

    def forwards(self, orm):
        "Popula bancos com base nas localizações já cadastradas"
        TYPES = (
            ('24', u'24 Horas'),
            ('santander', u'Santander'),
            ('nordeste', u'Banco do Nordeste'),
            ('caixa', u'Caixa Econômica'),
            ('bradesco', u'Bradesco'),
            ('itau', u'Itaú'),
            ('citybank', u'CityBank'),
            ('rural', u'Banco Rural'),
            ('brasil', u'Banco do Brasil'),
            ('hsbc', u'HSBC'),
            ('central', u'Banco Central'),
        )

        for slug, name in TYPES:
            b = orm['geo.BankType'].objects.create(name=name, slug=slug)
            b.save()

    def backwards(self, orm):
        "Write your backwards methods here."

    models = {
        'geo.bank': {
            'Meta': {'object_name': 'Bank'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.contrib.gis.db.models.fields.PointField', [], {'unique': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'geo.banktype': {
            'Meta': {'object_name': 'BankType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'blank': 'True'})
        }
    }

    complete_apps = ['geo']
    symmetrical = True
