# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Bank', fields ['location']
        db.create_unique('geo_bank', ['location'])


    def backwards(self, orm):
        # Removing unique constraint on 'Bank', fields ['location']
        db.delete_unique('geo_bank', ['location'])


    models = {
        'geo.bank': {
            'Meta': {'object_name': 'Bank'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.contrib.gis.db.models.fields.PointField', [], {'unique': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['geo']