from django.contrib import admin

from .models import Bank, BankType

class BankAdmin(admin.ModelAdmin):
    list_filter = ('bank',)

admin.site.register(Bank, BankAdmin)
admin.site.register(BankType)
