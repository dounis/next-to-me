# -*- coding: utf-8 -*-
from unicodedata import normalize
import json
import requests
import unicodedata

from django.utils.encoding import smart_unicode
from django.contrib.gis.geos import Point

from geo.models import Bank


def get_data():
    url = 'https://maps.googleapis.com/maps/api/place/search/json?location=-8.055416,-34.8881578&radius=9999&name=banco&sensor=false&key=AIzaSyALrYvYEkIS_R1KpDWWlWjGAOiB5cJFkUY'

    r = requests.get(url)
    print 'Nordeste' in r.content
    return json.loads(r.content)

def get_bank(name):
    name = smart_unicode(name)
    name = normalize('NFKD', name).encode('ascii','ignore')
    for i in Bank.TYPES:
        if i[0] in name.lower():
            return i[0]

def get_bank_instances():
    data = get_data()
    banks = []
    for r in data['results']:
        name = get_bank(r['name'])

        if name:
            points = r['geometry']['location']
            p = Point(points['lng'], points['lat'])
            b = Bank(name=name, location=p, address=r['vicinity'])
            if not Bank.objects.filter(location=p).exists():
                b.save()
