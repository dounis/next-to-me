from django.conf.urls import patterns, include, url

from django.contrib import admin

from tastypie.api import Api

from geo.api import BankResource, BankTypeResource


admin.autodiscover()

v1_api = Api(api_name='v1')
v1_api.register(BankTypeResource())
v1_api.register(BankResource())

urlpatterns = patterns('',
    (r'^', include('core.urls')),
    (r'^api/', include(v1_api.urls)),

    url(r'^admin/', include(admin.site.urls)),
)
