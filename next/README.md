API para Next-to-me
===================

GET
---
- http://host/api/v1/bank/?point={"coordinates": [-34.873058, -8.063426]}
- Traz todos bancos no raio de 50km 

- http://host/api/v1/bank/?point={"coordinates": [-34.873058, -8.063426]}&distance={"km": 0.2}')
- Traz todos bancos no raio definido

- ps.: Unidades suportadas: 
- https://docs.djangoproject.com/en/dev/ref/contrib/gis/measure/#supported-units

- Ex com filtro de banco:
- http://host/api/v1/bank/?point={"coordinates": [-34.873058, -8.063426]}&distance={"km": 0.2}&bank=4


- ps²: API um pouco inconsistente aqui, não segue o padrão do framework (tastypie) utilizado


POST (para cadastro)
--------------------
- Exemplo em Python:

```python
    import json
    import requests

    url = 'http://localhost:8000/api/v1/bank/'
    d = {
        u'address': u'Avenida X de Olinda, 215 - Recife Velho, Recife',
        u'location': {
            u'coordinates': [-34.87304, -8.063425], u'type': u'Point',
        },
        u'bank': {u'pk': 2},
    }

    headers = {'content-type': 'application/json'}
    requests.post(url, data=json.dumps(d), headers=headers)

```

- Notas:
- location tem que ser no formato indicado, com o atributo type
- Por hora o nome não é tratado, então é interessante seguir o padrão:

```python
    TYPES = (
        ('24', u'24 Horas'),
        ('santander', u'Santander'),
        ('nordeste', u'Banco do Nordeste'),
        ('caixa', u'Caixa Econômica'),
        ('bradesco', u'Bradesco'),
        ('itau', u'Itaú'),
        ('citybank', u'CityBank'),
        ('rural', u'Banco Rural'),
        ('brasil', u'Banco do Brasil'),
        ('hsbc', u'HSBC'),
        ('central', u'Banco Central'),
    )
```

