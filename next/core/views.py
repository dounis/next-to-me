from django.shortcuts import render
from geo.models import BankType


def index(request):
    banks = BankType.objects.all()

    bank = request.GET.get('bank')
    return render(request, 'index.html', {'banks': banks, 'bank': bank})
